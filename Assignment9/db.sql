DROP DATABASE IF EXISTS chat;
DROP USER IF EXISTS 'chat_user'@'localhost';
CREATE DATABASE chat CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER 'chat_user'@'localhost' IDENTIFIED BY 'chat_password';
GRANT ALL ON chat.* TO 'chat_user'@'localhost';

/* SCHEMAS */
CREATE TABLE chat.messages (
  username VARCHAR(15) NOT NULL,
  message VARCHAR(255) NOT NULL,
  sentAt DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

/* PROCEDURES */
DELIMITER //

/* Stores a message */
DROP PROCEDURE IF EXISTS chat.insert;
CREATE PROCEDURE chat.insert(username VARCHAR(15), message VARCHAR(255))
BEGIN
INSERT INTO chat.messages (username, message) VALUES (username, message);
END //

/* Retrieves up to ten most recent messages sent within the last hour */
DROP PROCEDURE IF EXISTS chat.getLastMessages;
CREATE PROCEDURE chat.getLastMessages()
BEGIN
SELECT * FROM chat.messages m
WHERE m.sentAt > DATE_SUB(NOW(), INTERVAL 1 HOUR)
ORDER BY m.sentAt DESC
LIMIT 10;
END //

/* Retrieves messages after a given time, representing all the new messages for a client */
DROP PROCEDURE IF EXISTS chat.getNewMessages;
CREATE PROCEDURE chat.getNewMessages(sentAt DATETIME)
BEGIN
SELECT * FROM chat.messages m
WHERE m.sentAt > sentAt
ORDER BY m.sentAt DESC;
END //

DELIMITER ;