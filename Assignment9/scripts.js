"use strict";

let username,
    usernameForm,
    messageForm,
    messageInput,
    messageButton,
    messagesBoard;

function prepareMessageBlock(message) {

    let html = '<article style="display:none" ';
    html += (message.username === username)? ' align="right">': '>';
    html += (message.username !== username)? '<b>' + message.username + '</b><br />': '';
    html += message.message + '<br /><span class="timestamp">' + message.sentAt + '</span><hr /></article>';

    return html;
}

function getLastMessages() {

    messagesBoard.html('');

    $.ajax("chat.php?action=get_last_messages")
        .done(function(messages) {
            $.each(JSON.parse(messages), function (i, message) {
                $(prepareMessageBlock(message)).appendTo(messagesBoard).show();
            })
        })
        .fail(function(){
            alert('Error: I suggest verifying the parameters of the database located in chat.php');
        });
}

function sendMessage() {

    messageForm.submit(function (e) {

        e.preventDefault();

        $.post("chat.php", {username: username, message: messageInput.val()})
            .done(function() {
                getLastMessages();
                messageInput.val('');
            })
    })
}

function getNewMessages() {

    let lastMessage = $('.timestamp').first().text();

    $.ajax("chat.php?action=get_new_messages&sent_at=" + lastMessage)
        .done(function(messages) {
            $.each(JSON.parse(messages), function (i, message) {
                $(prepareMessageBlock(message)).prependTo(messagesBoard).slideDown();
            })
        });
}

$(function() {

    usernameForm    = $('#username_form');
    messageForm     = $('#message_form');
    messagesBoard   = $('#messages_board');
    messageInput    = messageForm.find('#message');
    messageButton   = messageForm.find('button');

    messageInput.attr('disabled', true);
    messageButton.attr('disabled', true);

    usernameForm.submit(function (e) {

        e.preventDefault();

        username = usernameForm.find('#username').val();

        if(username.length > 0) {

            messageInput.attr('disabled', false);
            messageButton.attr('disabled', false);

            getLastMessages();
            sendMessage();
            setInterval(getNewMessages, 1000);
        }
    });
});