<?php

$db_name    = 'chat';
$username   = 'chat_user';
$password   = 'chat_password';
$db         = new PDO("mysql:host=localhost;dbname=$db_name;charset=utf8", $username, $password);

if($_GET) {
    if($_GET['action'] === 'get_last_messages') {
        $stmt = $db->prepare('CALL chat.getLastMessages();');
        $stmt->execute();
        echo json_encode($stmt->fetchAll(PDO::FETCH_CLASS));
    }
    elseif ($_GET['action'] === 'get_new_messages') {
        $stmt = $db->prepare('CALL chat.getNewMessages(:sentAt);');
        $stmt->execute([':sentAt' => $_GET['sent_at']]);
        echo json_encode($stmt->fetchAll(PDO::FETCH_CLASS));
    }
}

if($_POST) {
    $stmt = $db->prepare('CALL chat.insert(:username, :message);');
    echo json_encode($stmt->execute([':username' => $_POST['username'],':message' => $_POST['message']]));
}
