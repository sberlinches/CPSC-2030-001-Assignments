/*1. Find the Bottom 10 Pokémon ranked by speed*/
SELECT name, speed
FROM pokedex.pokemon
ORDER BY pokemon.speed ASC
LIMIT 10;


/*2. Find All Pokémon who is vulnerable to Ground and Resistant to Steel*/
SELECT p.name
FROM pokedex.pokemon AS p
INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
WHERE (qt.name = 'Vulnerable to' AND tt.name = 'Ground')
/* INTERSECT: Get only the Pokémon that has both conditions */
AND p.id IN (
  SELECT p.id
  FROM pokedex.pokemon AS p
  INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
  INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
  INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
  INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
  WHERE qt.name = 'Resistant to' AND tt.name = 'Steel'
);


/*3. Find All Pokémon who has a BST between 200 to 500 who is weak against water types*/
SELECT p.name, qt.name, tt.name, p.sum_base_values
FROM pokedex.pokemon AS p
INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
WHERE sum_base_values BETWEEN 200 AND 500 AND (qt.name = 'Weak against' AND tt.name = 'Water');


/*4. Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire*/
SELECT p2.name, p2.attack, qt.name, tt.name, p.name
FROM pokedex.pokemon p
/*Only the Pokémon with a Mega evolution*/
JOIN pokedex.pokemon p2 ON p.name LIKE CONCAT('Mega %', p2.name)
/*Only the Pokémon vulnerable to fire*/
INNER JOIN pokedex.pokemon_type AS pt on p2.id = pt.pokemonId
INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
WHERE qt.name = 'Vulnerable to' AND tt.name = 'Fire'
/*Only the Pokémon with the highest attack*/
ORDER BY p2.attack DESC
LIMIT 1;