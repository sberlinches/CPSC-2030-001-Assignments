DROP DATABASE IF EXISTS pokedex;
CREATE DATABASE pokedex CHARACTER SET utf8 COLLATE utf8_unicode_ci;

CREATE TABLE pokedex.pokemon (
  id SMALLINT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  national_number SMALLINT(6) UNSIGNED,
  hoenn_number SMALLINT(6) UNSIGNED,
  generation TINYINT(1) UNSIGNED NULL,
  popular BOOLEAN NOT NULL DEFAULT 0,
  name VARCHAR(50) NOT NULL,
  hp SMALLINT(6) UNSIGNED  NOT NULL,
  attack SMALLINT(6) UNSIGNED  NOT NULL,
  defense SMALLINT(6) UNSIGNED  NOT NULL,
  special_attack SMALLINT(6) UNSIGNED  NOT NULL,
  special_defense SMALLINT(6) UNSIGNED  NOT NULL,
  speed SMALLINT(6) UNSIGNED  NOT NULL,
  sum_base_values SMALLINT(6) UNSIGNED  NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY pokemon_name_uindex (name)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE pokedex.type (
  id SMALLINT(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY type_name_uindex (name)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE pokedex.pokemon_type (
  pokemonId SMALLINT(6) UNSIGNED NOT NULL,
  typeId SMALLINT(6) UNSIGNED NOT NULL,
  PRIMARY KEY (pokemonId,typeId),
  CONSTRAINT pokemon_type_pokemonId_fk 
    FOREIGN KEY (pokemonId) REFERENCES pokedex.pokemon (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT pokemon_type_typeId_fk 
    FOREIGN KEY (typeId) REFERENCES pokedex.type (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=latin1;

CREATE TABLE pokedex.quirkTag (
  id TINYINT(4) UNSIGNED NOT NULL,
  name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY type_name_uindex (name)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE pokedex.quirk (
  typeId SMALLINT(6) UNSIGNED NOT NULL,
  quirkTagId TINYINT(4) UNSIGNED NOT NULL,
  targetTypeId SMALLINT(6) UNSIGNED NOT NULL,
  PRIMARY KEY (typeId,quirkTagId,targetTypeId),
  CONSTRAINT quirk_typeId_fk
    FOREIGN KEY (typeId) REFERENCES pokedex.type (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT quirk_quirkTagId_fk
    FOREIGN KEY (quirkTagId) REFERENCES pokedex.quirkTag (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT quirk_targetTypeId_fk
    FOREIGN KEY (targetTypeId) REFERENCES pokedex.type (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=latin1;