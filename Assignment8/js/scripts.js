"use strict";

$(function() {

    let menuButton          = $("#menuButton");
    let cannonBall          = $("#cannonBall");
    let menuList            = $("#menu");
    let menuOptions         = $("#menu li a");
    let transitionDuration  = parseInt(menuButton.css('transition-duration')) * 1000;

    // Prevents the default behavior of the menu options when they are clicked
    menuOptions.click(function(event) {
        event.preventDefault();
    });

    // Every time the menu button is clicked:
    // Activates the aiming position or returns to the original position
    menuButton.click(function() {

        if(menuButton.hasClass("aiming")) {
            // Returns to the original position
            menuButton.removeClass();
            menuList.removeClass();
            // Removes all the events handlers from each option in the menu
            menuOptions.off();
            // Prevents the default behavior of the menu options when they are clicked
            menuOptions.click(function(event) {
                event.preventDefault();
            });
        }

        else {
            // Activates the aiming position
            menuButton.addClass("aiming");
            menuList.addClass("unfold");
            // Adds click even to each option in the menu
            menuOptions.click(function() {

                let href = $(this).attr('href');
                cannonBall.css({'left': $(this).offset().left});
                menuButton.removeClass("aiming");
                menuOptions.off();

                setTimeout(function() {
                    // Follows the link of the selected option
                    window.location.href = href;
                }, transitionDuration);
            });
        }
    });
});