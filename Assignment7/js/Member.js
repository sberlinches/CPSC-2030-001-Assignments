"use strict";

class Member {

    /**
     * Member class
     * @param name The name of the member
     * @param side The side of the member
     * @param unit The unit of the member
     * @param rank The rank of the member
     * @param role The role of the member
     * @param description The description of the member
     * @param picture The picture's path of the member
     */
    constructor(name, side, unit, rank, role, description, picture) {

        this.name = name;
        this.side = side;
        this.unit = unit;
        this.rank = rank;
        this.role = role;
        this.description = description;
        this.picture = picture;
        this.picture_path = 'http://valkyria.sega.com/img/character/';
    }

    /**
     * Returns the complete path of the picture of the member.
     * @returns {string} The complete path of the picture of the member
     */
    getPicture() {

        return this.picture_path + this.picture;
    }
}