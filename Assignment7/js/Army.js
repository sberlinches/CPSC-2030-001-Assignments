"use strict";

class Army {

    /**
     * Army class.
     * @param name The name of the army
     * @param battalionRoster The members of the battalion
     */
    constructor(name, battalionRoster) {

        this.name = name;
        this.battalionRoster = battalionRoster;
        this.squadRoster = [];
        this.maxSquadMembers = 5;
    }
}