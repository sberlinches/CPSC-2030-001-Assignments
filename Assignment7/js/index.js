"use strict";

window.onload = function() {

    let army = new Army(
        'Federate army',
        [
            new Member(
                'Minerva Victor',
                'Edinburgh Army',
                'Ranger Corps, Squad F',
                'First Lieutenant',
                'Senior Commander',
                'Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division\'s squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.',
                'img-chara11.png'
            ),
            new Member(
                'Dan Bentley',
                'Edinburgh Army',
                'Ranger Corps, Squad E',
                'Private First Class',
                'APC Operator',
                'Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.',
                'img-chara16.png'
            ),
            new Member(
                'Louffe',
                'Edinburgh Navy',
                'Centurion, Cygnus Fleet',
                'Sergeant',
                'Radar Operator',
                'As the Centurion\'s crewmember responsible for route reconnaissance, this young Darcsen has a keen intellect and acerbic wit beyond her age. Although her harsh tongue keeps most people at arm\'s length, she\'s developed a strong companionship with Marie Bennet.',
                'img-chara21.png'
            ),
            new Member(
                'Roland Morgen',
                'Edinburgh Navy',
                'Centurion, Cygnus Fleet',
                'Ship\'s Captain',
                'Cruiser Commander',
                'Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship\'s captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits.',
                'img-chara18.png'
            ),
            new Member(
                'Marie Bennett',
                'Edinburgh Navy',
                'Centurion, Cygnus Fleet',
                'Petty Officer',
                'Chief of Operations',
                'As the Centurion\'s crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.',
                'img-chara20.png'
            ),
            new Member(
                'Ronald Albee',
                'Edinburgh Army',
                'Ranger Corps, Squad F',
                'Second Lieutenant',
                'Tank Operator',
                'Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor\'s underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.',
                'img-chara17.png'
            ),
            new Member(
                'Kai Schulen',
                'Edinburgh Army',
                'Ranger Corps, Squad E',
                'Sergeant Major',
                'Fireteam Leader',
                'Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename "Deadeye Kai." Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.',
                'img-chara04.png'
            ),
            new Member(
                'Andre',
                'Edinburgh Navy',
                'Centurion, Cygnus Fleet',
                'Lieutenant',
                'Chief Engineer',
                'As the Centurion\'s crewmember responsible for maintenance and repairs, this EW1 veteran runs a tight ship. With a fondness for harsh language and hard liquor, André\'s overworked yet loyal mechanics fondly refer to him as the crew\'s grumpy old uncle.',
                'img-chara22.png'
            ),
            new Member(
                'Karen Stuart',
                'Edinburgh Army',
                'Squad E',
                'Corporal',
                'Combat EMT',
                'Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation\'s military to support her growing household.',
                'img-chara12.png'
            ),
            new Member(
                'Sergio',
                'Edinburgh Navy',
                'Centurion, Cygnus Fleet',
                'Lieutenant',
                'Chief Surgeon',
                'As the Centurion\'s crewmember responsible for the sick bay, this bodybuilding doctor was a small-town pediatrician before EW2. While kind and gentle at heart, his bedside manner is reserved for kids; grizzled vets have been reduced to tears by his tough love treatment.',
                'img-chara23.png'
            )
        ]
    );

    new Interface(
        army,
        document.getElementById('title'),
        document.getElementById('battalion_roster'),
        document.getElementById('squad_roster'),
        document.getElementById('member_profile')
    )
};