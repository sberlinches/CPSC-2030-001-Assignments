"use strict";

class Interface {

    /**
     * Interface class.
     * Manipulates the data and interaction of the interface.
     * @param army
     * @param titleContainer
     * @param battalionRosterContainer
     * @param squadRosterContainer
     * @param profileContainer
     */
    constructor(army, titleContainer, battalionRosterContainer, squadRosterContainer, profileContainer) {

        this.army = army;
        this.titleContainer = titleContainer;
        this.battalionRosterContainer = battalionRosterContainer;
        this.squadRosterContainer = squadRosterContainer;
        this.profileContainer = profileContainer;

        this.renderTitle();
        this.renderBattalionRoster();
    }

    renderTitle() {
        this.titleContainer.append(this.army.name);
    }

    /**
     * Renders the battalion roster on screen.
     */
    renderBattalionRoster() {

        let _this = this;
        let selected;

        for (let key in this.army.battalionRoster) {

            let li = document.createElement('li');
            li.append(document.createTextNode(this.army.battalionRoster[key].name));

            // Show member profile event
            li.addEventListener('mouseover', function() {
                _this.renderMemberProfile(_this.army.battalionRoster[key]);
            });

            // Add to squad roster event
            li.addEventListener('click', function(e) {
                if(_this.army.squadRoster.length < _this.army.maxSquadMembers &&
                    !_this.army.squadRoster.includes(_this.army.battalionRoster[key]))
                {

                    // Removes the class from the previous selected item
                    if(selected)
                        selected.classList.remove('selected');

                    selected = e.target;
                    selected.classList.add('selected');

                    _this.addToSquadRoster(_this.army.battalionRoster[key], _this.army.squadRoster);
                }
            });

            this.battalionRosterContainer.append(li);
        }
    }

    /**
     * Renders the member profile on screen
     * @param member The member object
     */
    renderMemberProfile(member) {

        this.profileContainer.innerHTML = '';

        // Attach the profile information
        for (let property in member) {

            if(property !== 'picture' && property !== 'picture_path') {
                let li = document.createElement('li');
                li.append(document.createTextNode(property + ': ' + member[property]));
                this.profileContainer.append(li);
            }
        }

        // Attach the picture
        let li = document.createElement('li');
        let img = document.createElement('img');
        img.src = member.getPicture();
        li.append(img);
        this.profileContainer.append(li);
    }

    /**
     * Renders the squad roster on screen
     */
    renderSquadRoster() {

        this.squadRosterContainer.innerHTML = '';

        let _this = this;

        for (let key in this.army.squadRoster) {

            let li = document.createElement('li');
            li.append(document.createTextNode(this.army.squadRoster[key].name));

            // Show member profile event
            li.addEventListener('mouseover', function() {
                _this.renderMemberProfile(_this.army.battalionRoster[key]);
            });

            // Remove from squad roster event
            li.addEventListener('click', function() {
                _this.removeFromSquadRoster(_this.army.squadRoster[key]);
            });

            this.squadRosterContainer.append(li);
        }
    }

    /**
     * Adds the member to the squad roster
     * @param member The member object
     */
    addToSquadRoster(member) {

        this.army.squadRoster.push(member);
        this.renderSquadRoster();
    }

    /**
     * Removes the member from the squad roster
     * @param member The member object
     */
    removeFromSquadRoster(member) {

        let index = this.army.squadRoster.indexOf(member);

        if (index > -1) {
            this.army.squadRoster.splice(index, 1);
            this.renderSquadRoster();
        }
    }
}