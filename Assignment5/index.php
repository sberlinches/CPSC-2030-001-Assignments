<?php

require_once ('Database.php');

$config     = include('config.php');
$db         = new Database($config->db);
$type       = (isset($_GET['type']))? $_GET['type']: null;
$pokemons   = ($type)?
    $db->getResults($config->procedures->getAllByType, ['type' => $type]):
    $db->getResults($config->procedures->getAll);
$title      = 'Pokédex';

function renderCards($pokemons) {
    foreach ($pokemons as $pokemon) {
        $name = strtolower($pokemon->name);
        echo '<div class="card">';
        echo "<h2><a href='detail.php?number=$pokemon->national_number&name=$name'title='$pokemon->national_number-$pokemon->name'>$pokemon->national_number-$pokemon->name</a></h2>";
        echo '<ul>';
        foreach (json_decode($pokemon->types) as $type) {
            $tag = strtolower($type);
            echo "<li><a class='tag $tag' href='?type=$tag'>$type</a></li>";
        }
        echo '</ul></div>';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>

<header>
    <h1><?php echo $title ?></h1>
    <?php if($type) echo "<a class='tag $type' href='/'>$type ×</a>" ?>
</header>

<main>
    <?php echo renderCards($pokemons) ?>
</main>

</body>
</html>
