DELIMITER //

DROP PROCEDURE IF EXISTS pokedex.getAll;
CREATE PROCEDURE pokedex.getAll()
  BEGIN
    SELECT p.id, p.national_number, p.name, JSON_ARRAYAGG(t.name) AS types
    #1.Gets all pokemon
    FROM pokedex.pokemon AS p
          #2.Gets all the types associated to each pokemon
          INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
          INNER JOIN pokedex.type AS t on pt.typeId = t.id
    GROUP BY p.id;
  END //

DROP PROCEDURE IF EXISTS pokedex.getAllByIds;
CREATE PROCEDURE pokedex.getAllByIds(IN ids VARCHAR(255))
  BEGIN
    SET @sql1 = 'SELECT p.id, p.national_number, p.name, JSON_ARRAYAGG(t.name) AS types ';
    SET @sql2 = 'FROM pokedex.pokemon AS p ';
    SET @sql3 = 'INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId ';
    SET @sql4 = 'INNER JOIN pokedex.type AS t on pt.typeId = t.id ';
    SET @sql5 = CONCAT('WHERE p.id IN (',`ids`,') ');
    SET @sql6 = 'GROUP BY p.id;';
    SET @sql = CONCAT(@sql1,@sql2,@sql3,@sql4,@sql5,@sql6);
    PREPARE s FROM @sql;
    EXECUTE s;
    DEALLOCATE PREPARE s;
  END //

DROP PROCEDURE IF EXISTS pokedex.getAllByType;
CREATE PROCEDURE pokedex.getAllByType(typeName VARCHAR(50))
  BEGIN
    SELECT p.id, p.national_number, p.name, JSON_ARRAYAGG(t2.name) AS types
    #1.Gets all pokemon that match with the given type
    FROM pokedex.type AS t
          INNER JOIN pokedex.pokemon_type AS pt ON t.id = pt.typeId
          INNER JOIN pokedex.pokemon AS p ON pt.pokemonId = p.id
          #2.Gets all the types associated to each pokemon
          INNER JOIN pokedex.pokemon_type AS pt2 on p.id = pt2.pokemonId
          INNER JOIN pokedex.type AS t2 on pt2.typeId = t2.id
    WHERE t.name = typeName
    GROUP BY p.id;
  END //


DROP PROCEDURE IF EXISTS pokedex.getById;
CREATE PROCEDURE pokedex.getById(id INT)
  BEGIN
    SELECT p.*, JSON_ARRAYAGG(t.name) AS types
    FROM pokedex.pokemon AS p
           INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
           INNER JOIN pokedex.type AS t on pt.typeId = t.id
    WHERE p.id = id;
  END //

DROP PROCEDURE IF EXISTS pokedex.getQuirksById;
CREATE PROCEDURE pokedex.getQuirksById(id INT)
  BEGIN
    SELECT qt.name AS tag, tt.name AS type
    FROM pokedex.pokemon AS p
           INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
           INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
           INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
           INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
    WHERE p.id = id;
  END //


DROP PROCEDURE IF EXISTS pokedex.getByNumberAndName;
CREATE PROCEDURE pokedex.getByNumberAndName(number INT, name VARCHAR(50))
  BEGIN
    SELECT p.*, JSON_ARRAYAGG(t.name) AS types
    FROM pokedex.pokemon AS p
           INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
           INNER JOIN pokedex.type AS t on pt.typeId = t.id
    WHERE p.national_number = number && p.name = name;
  END //


DROP PROCEDURE IF EXISTS pokedex.getQuirksByNumberAndName;
CREATE PROCEDURE pokedex.getQuirksByNumberAndName(number INT, name VARCHAR(50))
  BEGIN
    SELECT qt.name AS tag, tt.name AS type
    FROM pokedex.pokemon AS p
           INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
           INNER JOIN pokedex.quirk AS q on pt.typeId = q.typeId
           INNER JOIN pokedex.quirkTag qt on q.quirkTagId = qt.id
           INNER JOIN pokedex.type AS tt on q.targetTypeId = tt.id
    WHERE p.national_number = number && p.name = name;
  END //

DROP PROCEDURE IF EXISTS pokedex.getAllPopularFromGeneration;
CREATE PROCEDURE pokedex.getAllPopularFromGeneration(generation INT)
  BEGIN
    SELECT p.*, JSON_ARRAYAGG(t.name) AS types
    #1.Gets all pokemon
    FROM pokedex.pokemon AS p
      #2.Gets all the types associated to each pokemon
      INNER JOIN pokedex.pokemon_type AS pt on p.id = pt.pokemonId
      INNER JOIN pokedex.type AS t on pt.typeId = t.id
    WHERE p.generation = generation && p.popular = TRUE
    GROUP BY p.id;
  END //

DELIMITER ;