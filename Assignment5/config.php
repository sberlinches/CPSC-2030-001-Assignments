<?php

return (object) [
    'db' => (object) [
        'host' => 'localhost',
        'dbName' => 'pokedex',
        'username' => "CPSC2030",
        'password' => "CPSC2030",
    ],
    'procedures' => (object) [
        'getAll' => 'CALL pokedex.getAll()',
        'getAllByType' => 'CALL pokedex.getAllByType(:type)',
        'getByNumberAndName' => 'CALL pokedex.getByNumberAndName(:number, :name)',
        'getQuirksByNumberAndName' => 'CALL pokedex.getQuirksByNumberAndName(:number, :name)',
    ]
];
