<?php

class Database {

    private $db;

    /**
     * Database constructor.
     * @param $dbConfig
     */
    public function __construct($dbConfig) {

        $dsn = "mysql:host=$dbConfig->host;dbname=$dbConfig->dbName;charset=utf8";
        $options = [
            // turn off emulation mode for "real" prepared statements
            PDO::ATTR_EMULATE_PREPARES => false,
            // turn on errors in the form of exceptions
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];

        try {
            $this->db = new PDO($dsn, $dbConfig->username, $dbConfig->password, $options);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Prepares, executes, and returns an array of objects.
     * @param $query
     * @param array $params
     * @return mixed
     */
    public function getResults($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Prepares, executes, and returns an object.
     * @param $query
     * @param array $params
     * @return mixed
     */
    public function getResult($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchObject();
    }
}
