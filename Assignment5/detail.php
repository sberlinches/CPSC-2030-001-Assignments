<?php

if(!$_GET['number'] && !$_GET['name']) {
    echo 'Error: number and/or name parameters are missing';
    exit();
}

require_once ('Database.php');

$config     = include('config.php');
$number     = (int) $_GET['number'];
$name       = $_GET['name'];
$db         = new Database($config->db);
$pokemon    = $db->getResult($config->procedures->getByNumberAndName, [':number' => $number, 'name' => $name]);
$quirks     = $db->getResults($config->procedures->getQuirksByNumberAndName, [':number' => $number, 'name' => $name]);
$title      = 'Pokédex | ' . $pokemon->name;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" type="text/css" href="styles.css" />
</head>
<body>

<header>
    <h1><?php echo $title ?></h1>
    <a href="/">Go back</a>
</header>

<main>
    <div class="card big">
        <h2><?php echo $pokemon->national_number . '-' . $pokemon->name ?></h2>

        <ul>
        <?php
            foreach (json_decode($pokemon->types) as $type) {
                $tag = strtolower($type);
                echo "<li class='tag $tag'>$type</li>";
            }
        ?>
        </ul>

        <table>
            <tr>
                <th>National number:</th>
                <td><?php echo $pokemon->national_number ?></td>
            </tr>
            <tr>
                <th>Hoenn number:</th>
                <td><?php echo $pokemon->hoenn_number ?></td>
            </tr>
            <tr>
                <th>Health:</th>
                <td><?php echo $pokemon->hp ?></td>
            </tr>
            <tr>
                <th>Attack:</th>
                <td><?php echo $pokemon->attack ?></td>
            </tr>
            <tr>
                <th>Special attack:</th>
                <td><?php echo $pokemon->special_attack ?></td>
            </tr>
            <tr>
                <th>Defense:</th>
                <td><?php echo $pokemon->defense ?></td>
            </tr>
            <tr>
                <th>Special defense:</th>
                <td><?php echo $pokemon->special_defense ?></td>
            </tr>
            <tr>
                <th>Speed:</th>
                <td><?php echo $pokemon->speed ?></td>
            </tr>
            <tr>
                <th>Total:</th>
                <td><?php echo $pokemon->sum_base_values ?></td>
            </tr>
        </table>

        <ul>
        <?php
        foreach ($quirks as $quirk) {
            echo "<li>$quirk->tag - $quirk->type</li>";
        }
        ?>
        </ul>
    </div>
</main>

</body>
</html>