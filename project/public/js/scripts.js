"use strict";

/**
 * Creates a modal
 * @param triggerButton The button that creates the modal
 * @param header The HTML to show in the header
 * @param content The HTML to show in the content
 */
function createModal(triggerButton, header, content, acceptButton, cancelButton = false) {

    let modal               = $("#modal");
    let headerContainer     = modal.find("header");
    let contentContainer    = modal.find("section");
    let acceptAction        = $("#acceptAction");
    let cancelAction        = $("#cancelAction");

    triggerButton.click(function(event) {

        let href = $(this).prop('href');

        function resetModal() {

            modal.fadeOut();
            headerContainer.empty();
            contentContainer.empty();
            acceptAction.off();
            cancelAction.off();
        }

        event.preventDefault();
        headerContainer.html(header);
        contentContainer.html(content);
        modal.fadeIn();

        acceptAction.click(function () {
            window.location.href = href;
            resetModal();
        });

        cancelAction.click(function () {
            resetModal();
        });
    });
}

/**
 * Populates a select with the data from the given URL.
 * @param select The select to populate
 * @param apiUrl The relative URL to the API where to get the data
 * @param parentSelect (optional) The logic parent select
 * @param childSelects (optional) The logic child selects
 */
function populateSelect(select, apiUrl, parentSelect = null, childSelects = []) {

    let selectedId;

    // Empty the options in the dependent selects
    // E.g.: A city depends of the selected country.
    select.change(function () {

        selectedId = parseInt(select.find(":selected").val());

        $.each(childSelects, function(index, value) {

            value.html($('<option>'));

            // If the selected option is not empty, enables the first dependent
            // child only
            if(selectedId && index === 0)
                value.attr('disabled', false);

        });
    });

    // Populates the selected select
    select.mousedown(function () {

        let parentSelectedId = '';

        if(parentSelect)
            parentSelectedId = parseInt(parentSelect.find(":selected").val());

        if(parentSelect && !parentSelectedId)
            return;

        $.ajax(apiUrl + parentSelectedId)
            .done(function(result) {

                selectedId = parseInt(select.find(":selected").val());

                select.html($('<option>'));

                $.each(JSON.parse(result), function (index, value) {
                    select.append($('<option>', {
                        value: value.id,
                        text : value.name,
                        selected: (value.id === selectedId)
                    }));
                });
            })
    });
}

/**
 * Calculates and represents graphically the password strength of the given
 * input.
 * @param input
 */
function passwordStrengthMeter(input) {

    let meter = $("<div id='passwordStrengthMeter'><span></span></div>");

    input.after(meter);

    input.keyup(function () {

        let strength = 0;
        let password = $(this).val();

        if (password.length >= 4)
            strength++;
        if (password.length >= 6)
            strength++;
        if (password.match("(?=.*[a-z])"))
            strength++;
        if (password.match("(?=.*[A-Z])"))
            strength++;
        if (password.match("(?=.*[0-9])"))
            strength++;
        if (password.match("(?=.[!@#\\$%\\^&])"))
            strength++;

        meter.find('span').css("width", percentageOf(strength, 6) + '%');
    });
}

/**
 * Calculates and returns the percentage of a number over the total.
 * @param num
 * @param total
 * @returns {number}
 */
function percentageOf(num, total) {
    return (num/total)*100;
}

$(function() {

    // Creates the modal for the delete action
    createModal(
        $(".deleteAction"),
        '<h2>Hey!</h2>',
        '<p>Are you sure you want to delete it?</p>'
    );

    // Populates the country, state, and city selects
    populateSelect(
        $("#country"),
        "/country/api/get_all",
        null,
        [$("#state"), $("#city")]
    );

    populateSelect(
        $("#state"),
        "/state/api/get_all_by_country/",
        $("#country"),
        [$("#city")]
    );

    populateSelect(
        $("#city"),
        "/city/api/get_all_by_state/",
        $("#state")
    );

    // Calculates and shows the strength of the password
    passwordStrengthMeter($("#password"));


    // Magnifies the picture selected
    let pictures = $(".picture");

    $.each(pictures, function() {

        createModal(
            $(this),
            '',
            '<img src="' + $(this).attr('src') + '">'
        );
    });
});