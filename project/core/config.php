<?php

define('QUERY_SEPARATOR', '?');
define('PARAMETER_SEPARATOR', '/');
define('NAMESPACE_SEPARATOR', '\\');
define("ROOT", $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR);
define('CORE_ROOT', ROOT . 'core' . DIRECTORY_SEPARATOR);
define('APP_ROOT', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('UPLOADS_PATH', ROOT . 'public' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR);
define('USER_SESSION', 'user');
define('FW_EVERYBODY', 'everybody'); //Logged and non-logged users
define('FW_LOGGED', 'logged'); //Logged users only
define('FW_ANONYMOUS', 'anonymous'); //Non-logged users only
//RESPONSES
define('RESPONSE_FAIL', 0);
define('RESPONSE_OK', 1);

define('ADMIN', 1);
define('EMPLOYEE', 2);
define('CLIENT', 3);