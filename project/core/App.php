<?php

class App {

    /**
     * App constructor.
     */
    function __construct() {

        $this->autoload();
        $this->start();
    }

    /**
     * Classes autoloader.
     */
    private function autoload() {

        spl_autoload_register(function($namespace) {

            $namespace = str_replace(NAMESPACE_SEPARATOR, DIRECTORY_SEPARATOR, $namespace);

            if (file_exists(APP_ROOT . $namespace . '.php')) {
                $path = APP_ROOT . $namespace . '.php';
                include_once $path;
            }
            elseif (file_exists(CORE_ROOT . $namespace . '.php')) {
                $path = CORE_ROOT . $namespace . '.php';
                include_once $path;
            }
            else {
                echo 'AUTOLOAD ERROR: ' . $namespace . '<br>';
            }
        });
    }

    /**
     * Starts the application.
     */
    private function start() {

        require_once CORE_ROOT . 'Classes/UrlParser.php';
        require_once CORE_ROOT . 'Classes/Firewall.php';
        require_once APP_ROOT . 'AppRouter.php';

        session_start();

        $router = new UrlParser($_SERVER['REQUEST_URI']);
        $firewall = new Firewall($router->getParsedUrl());

        if ($firewall->checkRule())
            try {
                new AppRouter($router->getParsedUrl());
            } catch (Exception $e) {
                echo '<pre>';
                print_r($e);
                echo '</pre>';
            }
        else
            echo 'no access'; //TODO: Redirection
    }
}