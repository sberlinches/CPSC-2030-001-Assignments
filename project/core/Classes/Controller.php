<?php

namespace Classes;

use \Twig_Loader_Filesystem;
use \Twig_Environment;
use \Twig_Filter;
use \Twig_Function;

class Controller {

    protected
        $url,
        $config,
        $response,
        $userSession;

    /**
     * Controller constructor.
     * @param $url
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function __construct($url) {

        $this->url = $url;
        $this->config = include(APP_ROOT . 'config.php');
        $this->response = new Response();
        $this->userSession = isset($_SESSION[USER_SESSION])? $_SESSION[USER_SESSION]: null;

        // Checks if the user has permission in every controller call
        if(!$this->checkAccessControl(
            $this->config->modules[$this->url->module][$this->url->controller][$this->url->action],
            $this->url->parameter
        )) {
            $this->render('error.html.twig', [
                'uid' => 'error',
                'title' => '401: Unauthorized',
                'message' => '401: Unauthorized',
                'referer' => $_SERVER['HTTP_REFERER']
            ]);
        }
    }

    /**
     * Renders the template.
     * @param string $view: The HTML template path
     * @param $params: The parameters to pass to the template
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function render($view, $params = []) {

        // Initializes Twig
        $loader = new Twig_Loader_Filesystem(APP_ROOT);
        $twig = new Twig_Environment($loader);

        // Adds custom filters
        $twig->addFilter(new Twig_Filter('json_decode', function ($string) {
            return json_decode($string);
        }));

        // Adds custom functions
        $twig->addFunction(new Twig_Function('checkAccessControl', function ($module, $userId = null) {
            return $this->checkAccessControl($module, $userId);
        }));

        // Injects the basic variables to the template
        $params['modules'] = $this->config->modules;
        $params['userSession'] = (isset($_SESSION[USER_SESSION]))? $_SESSION[USER_SESSION]: null;

        echo $twig->render($view, $params);
        exit();
    }

    /**
     * Redirects to the provided address (Permanently by default).
     * @param string $address: The address to redirect
     * @param int $http_response_code: 301 by default
     */
    protected function redirect($address, $http_response_code = 301) {

        header('Location: ' . $address, true, $http_response_code);
        exit();
    }

    /**
     * Checks whether the user has access to the module or not.
     * @param $module
     * @param $userId
     * @return bool
     */
    protected function checkAccessControl($module, $userId) {

        if($module->firewall === FW_LOGGED) {

            if(isset($_SESSION[USER_SESSION])) {

                // Check if the user role has access to the module
                foreach ($module->accessControl as $roleId)
                    if($roleId === $_SESSION[USER_SESSION]->roleId)
                        return true;

                // The owner of the content has access always
                if($_SESSION[USER_SESSION]->id === $userId)
                    return true;
            }

            return false;
        }

        return true;
    }

    /**
     * @param $file
     * @param $validTypes
     * @param $path
     * @return mixed
     */
    protected function uploadFile($file, $validTypes, $path) {

        $filename = uniqid();

        // Check if the folder to store the uploaded file exists.
        // If doesn't exists, it is created
        if(!file_exists($path))
            if (!mkdir($path, 0777))
                throw new Exception('The folder could not be created');

        // Check if the file type is valid
        if (in_array(mime_content_type($file['tmp_name']), $validTypes))

            // Move the uploaded file from its temporal location to its final location
            if (move_uploaded_file($file['tmp_name'], $path . $filename))
                return $this->response->create(RESPONSE_OK, 'The file has been uploaded.', $filename);
            else
                return $this->response->create(RESPONSE_FAIL, 'There was an error uploading the file.');
        else
            return $this->response->create(RESPONSE_FAIL, 'The file type is not valid.');
    }
}