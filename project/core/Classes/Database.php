<?php

namespace Classes;

use PDO;

class Database {

    private $db;
    public $response;

    /**
     * Database constructor.
     * @param $host
     * @param $dbName
     * @param $username
     * @param $password
     */
    public function __construct($host, $dbName, $username, $password) {

        $dsn = "mysql:host=$host;dbname=$dbName;charset=utf8";
        $options = [
            // turn off emulation mode for "real" prepared statements
            PDO::ATTR_EMULATE_PREPARES => false,
            //turn on errors in the form of exceptions
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];

        try {
            $this->db = new PDO($dsn, $username, $password, $options);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }

        $this->response = new Response();
    }

    /**
     * Prepares, executes, and returns the last inserted id.
     * @param $query
     * @param array $params
     * @return int: The last inserted id
     */
    public function insert($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return (int) $this->db->lastInsertId();
    }

    /**
     * Prepares, executes, and returns the number of updated rows.
     * @param $query
     * @param array $params
     * @return int: The number of updated rows
     */
    public function update($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return (int) $stmt->rowCount();
    }

    /**
     * Prepares, executes, and returns whether the row has been deleted or not.
     * @param $query
     * @param array $params
     * @return bool
     */
    public function delete($query, $params = []) {

        $stmt = $this->db->prepare($query);
        return $stmt->execute($params);
    }

    /**
     * Prepares, executes, and returns an array of objects.
     * @param $query
     * @param array $params
     * @return mixed
     */
    public function getResults($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * Prepares, executes, and returns an object.
     * @param $query
     * @param array $params
     * @return mixed
     */
    public function getResult($query, $params = []) {

        $stmt = $this->db->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchObject();
    }
}