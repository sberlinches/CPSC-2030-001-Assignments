<?php

class Firewall {

    private $config, $url;

    /**
     * Firewall constructor.
     * @param $url: The parsed url
     */
    public function __construct($url) {

        $this->config = include(APP_ROOT . 'config.php');
        $this->url = $url;
    }

    /**
     * Checks and returns whether the firewall rule grants or not the access to
     * the requested action.
     * @return bool
     */
    public function checkRule() {

        $firewallRule = $this->getFirewallRule(
            $this->url->module,
            $this->url->controller,
            $this->url->action
        );

        switch ($firewallRule) {
            case FW_EVERYBODY:
                return true;
                break;
            case FW_LOGGED:
                return (isset($_SESSION[USER_SESSION]));
                break;
            case FW_ANONYMOUS:
                return (!isset($_SESSION[USER_SESSION]));
                break;
            default:
                return false;
        }
    }

    /**
     * Gets the firewall rule from the requested action.
     * @param $module
     * @param $controller
     * @param $action
     * @return mixed
     */
    private function getFirewallRule($module, $controller, $action) {

        if (!isset($this->config->modules[$module][$controller][$action])) {
            echo "Entry: 'modules' >  '$module' > '$controller' > '$action' not found in config.php";
            exit();
        }

        return $this->config->modules[$module][$controller][$action]->firewall;
    }
}