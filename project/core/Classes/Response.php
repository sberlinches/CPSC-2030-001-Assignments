<?php

namespace Classes;

class Response {

    /**
     * Response constructor.
     */
    public function __construct() {}

    /**
     * Creates a response.
     * @param $code
     * @param $message
     * @param null $data
     * @return object
     */
    public function create($code, $message, $data = null) {

        return (object) [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];
    }
}