<?php

class UrlParser {

    protected $config, $url, $parsedUrl;

    /**
     * UrlParser constructor.
     * @param $url: The url string
     */
    public function __construct($url) {

        $this->config = include(APP_ROOT . 'config.php');
        $this->url = $url;
        $this->parsedUrl = (object) [
            'module' => null,
            'controller' => 'default',
            'action' => 'index',
            'parameter' => null,
            'query' => null
        ];

        $this->parseUrl();
    }

    /**
     * Parses and breaks the url string into: module, controller, action, and
     * query.
     */
    private function parseUrl() {

        // 1. Extract the query string from the url
        $url = explode(QUERY_SEPARATOR, $this->url, 2);

        if (isset($url[1])) $this->parsedUrl->query = $url[1];

        // 2. Extract the parameters of the url
        $url = strtolower($url[0]);
        $url = ltrim($url, PARAMETER_SEPARATOR);
        $url = explode(DIRECTORY_SEPARATOR, $url);

        // 3. Set up the homepage module in case no module is provided
        if(!$url[0]) $url[0] = $this->config->homepage;

        if (isset($url[0])) $this->parsedUrl->module = $url[0];
        if (isset($url[1])) $this->parsedUrl->controller = $url[1];
        if (isset($url[2])) $this->parsedUrl->action = $url[2];
        if (isset($url[3])) $this->parsedUrl->parameter = (int)$url[3];
    }

    /**
     * Returns the parsed url.
     * @return object
     */
    public function getParsedUrl() {
        return $this->parsedUrl;
    }
}