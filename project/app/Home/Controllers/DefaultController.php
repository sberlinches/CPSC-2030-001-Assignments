<?php

namespace Home\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Appointments\Models\Appointment;
use Users\Models\User;

class DefaultController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'index':
                $this->indexAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function indexAction() {

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );

        $appointmentsModel = new Appointment($db);
        $userModel = new User($db);

        $this->render(
            $this->config->modules['home']['default']['index']->view, [
            'uid' => $this->config->modules['home']['default']['index']->uid,
            'title' => 'Home',
            'appointments' => $appointmentsModel->getPictures(4),
            'employees' => $userModel->getByRole(EMPLOYEE, 4)
        ]);
    }
}
