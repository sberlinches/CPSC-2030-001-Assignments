<?php

namespace Home;

use Exception;
use Home\Controllers\DefaultController;

class HomeRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'default':
                new DefaultController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}