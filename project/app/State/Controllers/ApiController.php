<?php

namespace State\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use State\Models\State;

class ApiController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'get_all_by_country':
                $this->getAllByCountryAction();
                break;
            default:
                throw new Exception( $url->action . 'Action not registered');
        }
    }

    private function getAllByCountryAction() {

        // Check if the URL params are present
        $countryId = $this->url->parameter;
        if (!$countryId)
            throw new Exception('Parameter missing');

        $stateModel = new State(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        echo json_encode($stateModel->getAllByCountryId($countryId));
    }
}
