<?php

namespace State;

use Exception;
use State\Controllers\ApiController;

class StateRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'api':
                new ApiController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}