<?php

namespace State\Models;

Use Classes\Database;

class State {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all states by country.
     * @param $countryId
     * @return mixed
     */
    public function getAllByCountryId($countryId) {

        return $this->db->getResults(
            "SELECT * FROM state WHERE countryId = :countryId",
            [':countryId' => $countryId]
        );
    }
}