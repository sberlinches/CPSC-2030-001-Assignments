<?php

use Home\HomeRouter;
use Authentication\AuthenticationRouter;
use Users\UsersRouter;
use Pets\PetsRouter;
use Appointments\AppointmentsRouter;
use Country\CountryRouter;
use State\StateRouter;
use City\CityRouter;

class AppRouter {

    public function __construct($url) {

        switch ($url->module) {
            case 'home':
                new HomeRouter($url);
                break;
            case 'authentication':
                new AuthenticationRouter($url);
                break;
            case 'users':
                new UsersRouter($url);
                break;
            case 'pets':
                new PetsRouter($url);
                break;
            case 'appointments':
                new AppointmentsRouter($url);
                break;
            case 'country':
                new CountryRouter($url);
                break;
            case 'state':
                new StateRouter($url);
                break;
            case 'city':
                new CityRouter($url);
                break;
            default:
                throw new Exception( $url->module . ' module not registered');
        }
    }
}