<?php

return (object) [
    'db' => [
        'mysql' => (object) [
            'host' => 'localhost',
            'dbName' => 'acicalado',
            'username' => "acicalado_user",
            'password' => "acicalado_password",
        ]
    ],
    'modules' => [
        'authentication' => [
            'login' => [
                'index' => (object) [
                    'uid' => 'login',
                    'url' => '/authentication/login',
                    'firewall' => FW_ANONYMOUS,
                    'accessControl' => [],
                    'view' => '/Authentication/Views/Login/index.html.twig',
                ]
            ],
            'logout' => [
                'index' => (object) [
                    'uid' => 'logout',
                    'url' => '/authentication/logout',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN,EMPLOYEE,CLIENT],
                ]
            ],
            'sign_up' => [
                'index' => (object) [
                    'uid' => 'sign_up',
                    'url' => '/authentication/sign_up',
                    'firewall' => FW_ANONYMOUS,
                    'accessControl' => [],
                    'view' => '/Authentication/Views/SignUp/index.html.twig',
                ]
            ],
        ],
        'home' => [
            'default' => [
                'index' => (object) [
                    'uid' => 'home',
                    'url' => '/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Home/Views/Default/index.html.twig',
                ]
            ]
        ],
        'users' => [
            'profile' => [
                'list' => (object) [
                    'uid' => 'users_profile_list',
                    'url' => '/users/profile/list',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN,EMPLOYEE],
                    'view' => '/Users/Views/Profile/list.html.twig',
                ],
                'new' => (object) [
                    'uid' => 'users_profile_new',
                    'url' => '/users/profile/new/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Users/Views/Profile/new.html.twig',
                ],
                'show' => (object) [
                    'uid' => 'users_profile_show',
                    'url' => '/users/profile/show/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Users/Views/Profile/show.html.twig',
                ],
                'edit' => (object) [
                    'uid' => 'users_profile_edit',
                    'url' => '/users/profile/edit/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Users/Views/Profile/edit.html.twig',
                ],
                'delete' => (object) [
                    'uid' => 'users_profile_delete',
                    'url' => '/users/profile/delete/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                ]
            ],
            'appointments' => [
                'list' => (object) [
                    'uid' => 'users_appointments_list',
                    'url' => '/users/appointments/list/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Users/Views/Appointments/index.html.twig',
                ],
            ],
            'pets' => [
                'list' => (object) [
                    'uid' => 'users_pets_list',
                    'url' => '/users/pets/list/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Users/Views/Pets/index.html.twig',
                ],
            ],
            'groomers' => [
                'list' => (object) [
                    'uid' => 'users_groomers_list',
                    'url' => '/users/groomers/list/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Users/Views/Groomers/index.html.twig',
                ],
            ]
        ],
        'pets' => [
            'profile' => [
                'list' => (object) [
                    'uid' => 'pets_profile_list',
                    'url' => '/pets/profile/list',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Pets/Views/Profile/list.html.twig',
                ],
                'show' => (object) [
                    'uid' => 'pets_profile_show',
                    'url' => '/pets/profile/show/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Pets/Views/Profile/show.html.twig',
                ],
                'new' => (object) [
                    'uid' => 'pets_profile_new',
                    'url' => '/pets/profile/new/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Pets/Views/Profile/new.html.twig',
                ],
                'edit' => (object) [
                    'uid' => 'pets_profile_edit',
                    'url' => '/pets/profile/edit/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                    'view' => '/Pets/Views/Profile/edit.html.twig',
                ],
                'delete' => (object) [
                    'url' => '/pets/profile/delete/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN],
                ]
            ],
            'gallery' => [
                'list' => (object) [
                    'uid' => 'pets_gallery_list',
                    'url' => '/pets/gallery/list/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Pets/Views/Gallery/list.html.twig',
                ]
            ]
        ],
        'appointments' => [
            'default' => [
                'list' => (object) [
                    'uid' => 'appointments_list',
                    'url' => '/appointments/default/list',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN,EMPLOYEE],
                    'view' => '/Appointments/Views/Default/list.html.twig',
                ],
                'new' => (object) [
                    'uid' => 'appointments_new',
                    'url' => '/appointments/default/new/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [ADMIN,EMPLOYEE],
                    'view' => '/Appointments/Views/Default/new.html.twig',
                ],
                'edit' => (object) [
                    'uid' => 'appointments_edit',
                    'url' => '/appointments/default/edit/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN,EMPLOYEE],
                    'view' => '/Appointments/Views/Default/edit.html.twig',
                ],
                'manage' => (object) [
                    'uid' => 'appointments_manage',
                    'url' => '/appointments/default/manage/',
                    'firewall' => FW_LOGGED,
                    'accessControl' => [ADMIN,EMPLOYEE],
                    'view' => '/Appointments/Views/Default/manage.html.twig',
                ]
            ],
            'gallery' => [
                'list' => (object) [
                    'uid' => 'appointments_gallery_list',
                    'url' => '/appointments/gallery/list',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                    'view' => '/Appointments/Views/Gallery/list.html.twig',
                ],
            ]
        ],
        'country' => [
            'api' => [
                'get_all' => (object) [
                    'url' => '/country/api/get_all',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                ]
            ]
        ],
        'state' => [
            'api' => [
                'get_all_by_country' => (object) [
                    'url' => '/state/api/get_all_by_country/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                ]
            ]
        ],
        'city' => [
            'api' => [
                'get_all_by_state' => (object) [
                    'url' => '/city/api/get_all_by_state/',
                    'firewall' => FW_EVERYBODY,
                    'accessControl' => [],
                ]
            ]
        ],
    ],
    'homepage' => 'home'
];