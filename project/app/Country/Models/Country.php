<?php

namespace Country\Models;

Use Classes\Database;

class Country {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all countries.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT * FROM country"
        );
    }
}