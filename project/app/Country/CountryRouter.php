<?php

namespace Country;

use Exception;
use Country\Controllers\ApiController;

class CountryRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'api':
                new ApiController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}