<?php

namespace Country\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Country\Models\Country;

class ApiController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'get_all':
                $this->getAllAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function getAllAction() {

        $countryModel = new Country(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        echo json_encode($countryModel->getAll());
    }
}
