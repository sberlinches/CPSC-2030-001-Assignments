<?php

namespace Pets\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Appointments\Models\Appointment;
use Pets\Models\Pet;

class GalleryController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Check if the URL params are present
        $petId = $this->url->parameter;
        if (!$petId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );

        $appointmentModel = new Appointment($db);
        $petModel = new Pet($db);
        $pet = $petModel->getById($petId);

        // Renders the view
        $this->render(
            $this->config->modules['pets']['gallery']['list']->view, [
            'uid' =>  $this->config->modules['pets']['gallery']['list']->uid,
            'title' => $pet->name . '\'s gallery',
            'petId' => $petId,
            'appointments' => $appointmentModel->getPicturesByPetId($petId)
        ]);
    }
}