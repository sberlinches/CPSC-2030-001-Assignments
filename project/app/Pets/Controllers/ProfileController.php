<?php

namespace Pets\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Pets\Models\Pet;
use PetTypes\Models\PetType;

class ProfileController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            case 'show':
                $this->showAction();
                break;
            case 'new':
                $this->newAction();
                break;
            case 'edit':
                $this->editAction();
                break;
            case 'delete':
                $this->deleteAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Initializes DB, models and variables
        $petModel = new Pet(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        // Renders the view
        $this->render(
            $this->config->modules['pets']['profile']['list']->view, [
            'uid' =>  $this->config->modules['pets']['profile']['list']->uid,
            'title' => 'Pets',
            'pets' => $petModel->getAll()
        ]);
    }

    private function showAction() {

        // Check if the URL params are present
        $petId = $this->url->parameter;
        if (!$petId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $petModel = new Pet(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));
        $pet = $petModel->getById($petId);

        // Check if the pet exists
        if(!$pet)
            $this->render('error.html.twig', [
                'uid' => 'error',
                'message' => '404: Not found'
            ]);

        // Renders the view
        $this->render(
            $this->config->modules['pets']['profile']['show']->view, [
            'uid' =>  $this->config->modules['pets']['profile']['show']->uid,
            'title' => $pet->name . '\'s profile',
            'petId' => $petId,
            'pet' => $pet
        ]);

    }

    private function newAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $petModel = new Pet($db);
        $petTypeModel = new PetType($db);

        // Stores in DB and redirects
        if($_POST) {

            $_POST['ownerId'] = $userId;
            $pet = (object) $_POST;

            //4.2. Checks if the form is valid and updates
            $response = $petModel->create($pet);

            //3.1 If the update succeed redirects, otherwise it will show errors
            if ($response->code === RESPONSE_OK)
                $this->redirect($this->config->modules['users']['pets']['list']->url . $userId);
        }

        // Renders the view
        $this->render(
            $this->config->modules['pets']['profile']['new']->view, [
            'uid' =>  $this->config->modules['pets']['profile']['new']->uid,
            'title' => 'New pet profile',
            'response' => (isset($response))? $response: null,
            'userId' => $userId,
            'petTypes' => $petTypeModel->getAll(),
            'pet' => (isset($pet))? $pet: null
        ]);
    }

    private function editAction() {

        // Check if the URL params are present
        $petId = $this->url->parameter;
        if (!$petId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $petModel = new Pet($db);
        $petTypeModel = new PetType($db);

        // Stores in DB and redirects
        if ($_POST || $_FILES) {

            $_POST['id'] = $petId;
            $pet = (object) $_POST;

            //4.1. Checks if the form is valid and updates
            $response = $petModel->update($pet);

            //4.2 If the update succeed redirects, otherwise it will show errors
            if ($response->code === RESPONSE_OK)
                $this->redirect($this->config->modules['pets']['profile']['show']->url . $pet->id);

        } else
            $pet = $petModel->getById($petId);

        // Check if the pet exists
        if(!$pet)
            $this->render('error.html.twig', [
                'uid' => 'error',
                'message' => '404: Not found'
            ]);

        // Renders the view
        $this->render(
            $this->config->modules['pets']['profile']['edit']->view, [
            'uid' =>  $this->config->modules['pets']['profile']['edit']->uid,
            'title' => 'Edit ' . $pet->name . '\'s profile',
            'petId' => $petId,
            'pet' => $pet,
            'petTypes' => $petTypeModel->getAll(),
            'response' => (isset($response))? $response: null
        ]);
    }

    private function deleteAction() {

        // Check if the URL params are present
        $petId = $this->url->parameter;
        if (!$petId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $petModel = new Pet(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        $response = $petModel->delete($petId);

        if($response->code === RESPONSE_OK) {
            if (isset($_SERVER['HTTP_REFERER']))
                $this->redirect($_SERVER['HTTP_REFERER']);
            else
                $this->redirect($this->config->modules['users']['pets']['list']->url);
        }
    }
}
