<?php

namespace Pets;

use Exception;
use Pets\Controllers\ProfileController;
use Pets\Controllers\GalleryController;

class PetsRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'profile':
                new ProfileController($url);
                break;
            case 'gallery':
                new GalleryController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}