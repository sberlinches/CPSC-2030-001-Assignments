<?php

namespace Pets\Models;

Use Classes\Database;

class Pet {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all pets.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT * FROM pet"
        );
    }

    /**
     * Gets and returns a pet by id.
     * @param $id
     * @return mixed
     */
    public function getById($id) {

        return $this->db->getResult(
            "SELECT p.*, t.name AS type, o.firstName AS ownerName, a.pictureAfter AS picture
            FROM pet p
            INNER JOIN petType t ON p.typeId = t.id
            INNER JOIN user o ON p.ownerId = o.id
            LEFT JOIN appointment a on p.id = a.petId && a.pictureAfter IS NOT NULL
            WHERE p.id = :id
            ORDER BY a.datetime DESC
            LIMIT 1",
            [':id' => $id]
        );
    }

    /**
     * Gets and returns all pets by owner.
     * @param $ownerId
     * @return mixed
     */
    public function getByOwnerId($ownerId) {

        return $this->db->getResults(
            "SELECT * FROM pet WHERE ownerId = :ownerId",
            [':ownerId' => $ownerId]
        );
    }

    /**
     * Inserts and returns the id if the user was inserted.
     * @param $pet
     * @return mixed
     */
    public function create($pet) {

        //1. Checks the required fields
        if(!$pet->ownerId)
            return $this->db->response->create(RESPONSE_FAIL, 'First name is required');
        if(!$pet->typeId)
            return $this->db->response->create(RESPONSE_FAIL, 'First name is required');
        if(!$pet->name)
            return $this->db->response->create(RESPONSE_FAIL, 'First name is required');

        //2. Inserts and returns the id
        return $this->db->response->create(RESPONSE_OK, 'Pet created',
            $this->db->insert(
                "INSERT INTO pet (ownerId, typeId, name, birthAt) VALUES (:ownerId, :typeId, :name, :birthAt)",
                [
                    ':ownerId' => $pet->ownerId,
                    ':typeId' => $pet->typeId,
                    ':name' => $pet->name,
                    ':birthAt' => ($pet->birthAt)? $pet->birthAt: null
                ]
            )
        );
    }

    /**
     * Updates the pet and returns the number of updated rows.
     * @param $pet
     * @return object
     */
    public function update($pet) {

        $query = 'UPDATE pet SET ';
        $params = [':id' => $pet->id];

        //1. Check fields and creates creates the query
        //Required fields
        if (isset($pet->name)) {
            if(!$pet->name)
                return $this->db->response->create(RESPONSE_FAIL, 'Name is required');

            $query.= 'name=:name,';
            $params[':name'] = $pet->name;
        }
        if (isset($pet->typeId)) {
            if(!$pet->typeId)
                return $this->db->response->create(RESPONSE_FAIL, 'Type is required');

            $query.= 'typeId=:typeId,';
            $params[':typeId'] = $pet->typeId;
        }
        // Optional fields
        if (isset($pet->birthAt) && $pet->birthAt) {
            $query.= 'birthAt=:birthAt,';
            $params[':birthAt'] = $pet->birthAt;
        }

        $query = rtrim($query, ',');
        $query.= ' WHERE id=:id';

        $res = $this->db->update($query, $params);

        return $this->db->response->create(RESPONSE_OK, 'Changes saved', $res);
    }

    /**
     * Deletes an user from the DB and returns whether the entry was deleted or
     * not.
     * @param $id
     * @return object
     */
    public function delete($id) {

        $res = $this->db->delete(
            "DELETE FROM pet WHERE id = :id",
            [':id' => $id]
        );

        if(!$res)
            return $this->db->response->create(RESPONSE_FAIL, 'The pet could not be deleted', $res);

        return $this->db->response->create(RESPONSE_OK, 'Pet deleted', $res);
    }
}