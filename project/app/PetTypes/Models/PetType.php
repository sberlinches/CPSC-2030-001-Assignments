<?php

namespace PetTypes\Models;

Use Classes\Database;

class PetType {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all pet types.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT * FROM petType"
        );
    }
}