<?php

namespace Authentication;

use Exception;
use Authentication\Controllers\LoginController;
use Authentication\Controllers\LogoutController;
use Authentication\Controllers\SignUpController;

class AuthenticationRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'login':
                new LoginController($url);
                break;
            case 'logout':
                new LogoutController($url);
                break;
            case 'sign_up':
                new SignUpController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}