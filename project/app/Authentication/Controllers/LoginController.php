<?php

namespace Authentication\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Users\Models\User;

class LoginController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'index':
                $this->indexAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function indexAction() {

        $response = [];

        if($_POST) {
            $response = $this->checkLoginForm($_POST);

            if ($response->code === RESPONSE_OK) {

                if($this->url->query === 'ref=new_appointment')
                    $this->redirect($this->config->modules['appointments']['default']['new']->url);
                else
                    $this->redirect($this->config->modules['users']['profile']['show']->url . $_SESSION[USER_SESSION]->id);
            }
        }

        $this->render(
            $this->config->modules['authentication']['login']['index']->view, [
                'uid' => $this->config->modules['authentication']['login']['index']->uid,
                'title' => 'Login',
                'query' => '?' . $this->url->query,
                'response' => $response
            ]);
    }

    private function checkLoginForm($form) {

        // Checks for the required fields
        if($form['email'] && $form['password']) {

            //1.1. Creates the connection
            $user = new User(new Database(
                $this->config->db['mysql']->host,
                $this->config->db['mysql']->dbName,
                $this->config->db['mysql']->username,
                $this->config->db['mysql']->password
            ));

            // Gets the user that the email matches
            $user = $user->getByEmail($form['email']);

            // If the user exists
            if ($user) {

                // Checks the password
                if(password_verify($form['password'], $user->password)) {

                    $_SESSION[USER_SESSION] = (object)[
                        'id' => $user->id,
                        'email' => $user->email,
                        'roleId' => $user->roleId,
                        'firstName' => $user->firstName
                    ];

                    return $this->response->create(RESPONSE_OK, 'Welcome back!');
                } else
                    return $this->response->create(RESPONSE_FAIL, 'Password doesn\'t match.');
            } else
                return $this->response->create(RESPONSE_FAIL, 'Email doesn\'t match.');
        } else
            return $this->response->create(RESPONSE_FAIL, 'Check required fields.');
    }
}