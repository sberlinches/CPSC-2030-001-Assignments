<?php

namespace Authentication\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Users\Models\User;

class SignUpController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'index':
                $this->indexAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function indexAction() {

        // Initializes DB, models and variables
        $userModel = new User(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        // Stores in DB, login, and redirects
        if($_POST) {

            $_POST['roleId'] = CLIENT;
            $user = (object)$_POST;

            //4.2. Checks if the form is valid and updates
            $response = $userModel->create($user);

            //3.1 If the creation succeed log the user in and redirects
            if ($response->code === RESPONSE_OK) {

                $user = $userModel->getById($response->data);

                $_SESSION[USER_SESSION] = (object)[
                    'id' => $user->id,
                    'email' => $user->email,
                    'roleId' => $user->roleId,
                    'firstName' => $user->firstName
                ];

                $this->redirect($this->config->modules['users']['profile']['show']->url . $_SESSION[USER_SESSION]->id);
            }
        }

        // Renders the view
        $this->render(
            $this->config->modules['authentication']['sign_up']['index']->view, [
                'uid' => $this->config->modules['authentication']['sign_up']['index']->uid,
                'title' => 'Sign up',
                'user' => (isset($user))? $user: null,
                'response' => (isset($response))? $response: null
            ]);
    }
}