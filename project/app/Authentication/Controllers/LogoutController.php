<?php

namespace Authentication\Controllers;

use Exception;
use Classes\Controller;

class LogoutController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'index':
                $this->indexAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function indexAction() {

        // Empty and destroy the session
        $_SESSION = [];
        session_destroy();

        // Redirects to the homepage
        $this->redirect($this->config->modules['home']['default']['index']->url);
    }
}