<?php

namespace Appointments;

use Appointments\Controllers\GalleryController;
use Exception;
use Appointments\Controllers\DefaultController;

class AppointmentsRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'default':
                new DefaultController($url);
                break;
            case 'gallery':
                new GalleryController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}