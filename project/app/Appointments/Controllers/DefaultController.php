<?php

namespace Appointments\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Appointments\Models\Appointment;
use Pets\Models\Pet;
use Users\Models\User;

class DefaultController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            case 'new':
                $this->newAction();
                break;
            case 'edit':
                $this->editAction();
                break;
            case 'manage':
                $this->manageAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Initializes DB, models and variables
        $appointmentsModel = new Appointment(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        // Queries based on the user's role
        switch ($this->userSession->roleId) {
            case ADMIN:
                $appointments = $appointmentsModel->getAll();
                break;
            case EMPLOYEE:
                $appointments = $appointmentsModel->getByEmployeeId($this->userSession->id);
                break;
        }

        // Renders the view
        $this->render(
            $this->config->modules['appointments']['default']['list']->view, [
            'uid' =>  $this->config->modules['appointments']['default']['list']->uid,
            'title' => 'Appointments',
            'appointments' => $appointments
        ]);
    }

    private function newAction() {

        if(!$this->userSession)
            $this->redirect($this->config->modules['authentication']['login']['index']->url . '?ref=new_appointment');

        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );

        $petModel = new Pet($db);
        $userModel = new User($db);

        // Get the who is suppose to be the client
        $clientId = ($this->url->parameter)? $this->url->parameter: $this->userSession->id;

        if ($_POST) {

            $_POST['clientId'] = $clientId;
            $appointment = (object) $_POST;

            $appointmentModel = new Appointment($db);

            //4.1. Checks if the form is valid and creates
            $response = $appointmentModel->create($appointment);

            //4.2 If the update succeed redirects, otherwise it will show errors
            if ($response->code === RESPONSE_OK)
                $this->redirect($this->config->modules['users']['appointments']['list']->url . $clientId);
        }

        // Renders the view
        $this->render(
            $this->config->modules['appointments']['default']['new']->view, [
            'uid' =>  $this->config->modules['appointments']['default']['new']->uid,
            'title' => 'New appointment',
            'clientId' => $clientId,
            'pets' => $petModel->getByOwnerId($clientId),
            'employees' => $userModel->getByRole(EMPLOYEE),
            'appointment' => (isset($appointment))? $appointment: null,
            'response' => (isset($response))? $response: null
        ]);
    }

    private function editAction() {

        // Check if the URL params are present
        $appointmentId = $this->url->parameter;
        if (!$appointmentId)
            throw new Exception('Parameter missing');

        // Creates the variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );

        $appointmentModel = new Appointment($db);
        $userModel = new User($db);
        $petModel = new Pet($db);
        $appointment = $appointmentModel->getById($appointmentId);

        // Updates the data
        if($_POST) {

            $appointment->employeeId = $_POST["employeeId"];
            $appointment->petId = $_POST["petId"];
            $appointment->datetime = $_POST["datetime"];

            // Checks if the form is valid and updates
            $response = $appointmentModel->update($appointment);

            // If the update succeed redirects, otherwise it will show errors
            if ($response->code === RESPONSE_OK)
                $this->redirect($this->config->modules['appointments']['default']['list']->url);
        }

        // Renders the view
        $this->render(
            $this->config->modules['appointments']['default']['edit']->view, [
            'uid' =>  $this->config->modules['appointments']['default']['edit']->uid,
            'title' => 'Edit appointment',
            'pets' => $petModel->getByOwnerId($appointment->clientId),
            'employees' => $userModel->getByRole(EMPLOYEE),
            'appointment' => $appointment,
            'response' => (isset($response))? $response: null
        ]);
    }

    private function manageAction() {

        // Check if the URL params are present
        $appointmentId = $this->url->parameter;
        if (!$appointmentId)
            throw new Exception('Parameter missing');

        $appointmentModel = new Appointment(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        $appointment = $appointmentModel->getById($appointmentId);

        // Uploads the files
        if ($_FILES) {

            $validTypes = ['image/jpeg'];
            $pictureBefore = $_FILES["pictureBefore"];
            $pictureAfter = $_FILES["pictureAfter"];
            $update = true;

            if (!$pictureBefore['error']) {

                $responseBefore = $this->uploadFile($pictureBefore, $validTypes, UPLOADS_PATH);

                if ($responseBefore->code === RESPONSE_OK)
                    $appointment->pictureBefore = $responseBefore->data;
                else
                    $update = false;
            }

            if (!$pictureAfter['error']) {

                $responseAfter = $this->uploadFile($pictureAfter, $validTypes, UPLOADS_PATH);

                if ($responseAfter->code === RESPONSE_OK)
                    $appointment->pictureAfter = $responseAfter->data;
                else
                    $update = false;
            }

            if($update) {
                $response = $appointmentModel->update($appointment);

                if($response->code === RESPONSE_OK)
                    $this->redirect($this->config->modules['appointments']['default']['list']->url);
            }
        }

        // Renders the view
        $this->render(
            $this->config->modules['appointments']['default']['manage']->view, [
            'uid' =>  $this->config->modules['appointments']['default']['manage']->uid,
            'title' => 'Manage appointment',
            'appointment' => $appointment,
            'response' => (isset($response))? $response: null
        ]);
    }
}