<?php

namespace Appointments\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Appointments\Models\Appointment;

class GalleryController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Initializes DB, models and variables
        $appointmentsModel = new Appointment(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        $this->render(
            $this->config->modules['appointments']['gallery']['list']->view, [
            'uid' =>  $this->config->modules['appointments']['gallery']['list']->uid,
            'title' => 'Gallery',
            'appointments' => $appointmentsModel->getPictures()
        ]);
    }
}