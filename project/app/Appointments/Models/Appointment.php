<?php

namespace Appointments\Models;

Use Classes\Database;

class Appointment {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all appointments.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT a.*, e.firstName AS employeeName, c.firstName AS clientName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN user AS c ON c.id = a.clientId
            INNER JOIN pet AS p ON p.id = a.petId
            ORDER BY a.datetime DESC;"
        );
    }

    public function getById($id) {

        return $this->db->getResult(
            "SELECT a.*, e.firstName AS employeeName, c.firstName AS clientName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN user AS c ON c.id = a.clientId
            INNER JOIN pet AS p ON p.id = a.petId
            WHERE a.id = :id
            ORDER BY a.datetime DESC;",
            [':id' => $id]
        );
    }

    /**
     * Gets and returns all appointments by user.
     * @param $clientId
     * @return array
     */
    public function getByUserId($clientId) {

        return $this->db->getResults(
            "SELECT a.*, e.firstName AS employeeName, c.firstName AS clientName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN user AS c ON c.id = a.clientId
            INNER JOIN pet AS p ON p.id = a.petId
            WHERE a.clientId = :clientId
            ORDER BY a.datetime DESC;",
            [':clientId' => $clientId]
        );
    }

    /**
     * Gets and returns all appointments by employee.
     * @param $employeeId
     * @return array
     */
    public function getByEmployeeId($employeeId) {

        return $this->db->getResults(
            "SELECT a.*, e.firstName AS employeeName, c.firstName AS clientName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN user AS c ON c.id = a.clientId
            INNER JOIN pet AS p ON p.id = a.petId
            WHERE a.employeeId = :employeeId
            ORDER BY a.datetime DESC;",
            [':employeeId' => $employeeId]
        );
    }

    /**
     * Gets and returns the pet's before/after pictures.
     * (Only the ones with the after picture)
     * @param $petId
     * @return mixed
     */
    public function getPicturesByPetId($petId) {

        return $this->db->getResults(
            "SELECT a.*, e.firstName AS employeeName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN pet AS p ON p.id = a.petId
            WHERE a.petId = :petId && a.pictureAfter IS NOT NULL
            ORDER BY a.datetime DESC;",
            [':petId' => $petId]
        );
    }

    /**
     * Gets and returns all completed appointments which have a picture.
     * (An appointment is considered completed when there's an after picture)
     * @param $limit
     * @return mixed
     */
    public function getPictures($limit = null) {

        $query = "SELECT a.*, e.firstName AS employeeName, p.name AS petName
            FROM appointment a
            INNER JOIN user AS e ON e.id = a.employeeId
            INNER JOIN pet AS p ON p.id = a.petId
            WHERE a.pictureAfter IS NOT NULL
            ORDER BY a.datetime DESC";

        if($limit)
            $query .= ' LIMIT ' . $limit;

        return $this->db->getResults($query);
    }

    /**
     * Inserts and returns the id if the appointment was inserted.
     * @param $appointment
     * @return mixed
     */
    public function create($appointment) {

        //1. Checks the required fields
        if(!$appointment->employeeId)
            return $this->db->response->create(RESPONSE_FAIL, 'Employee is required');
        if(!$appointment->clientId)
            return $this->db->response->create(RESPONSE_FAIL, 'Owner is required');
        if(!$appointment->petId)
            return $this->db->response->create(RESPONSE_FAIL, 'Pet is required');
        if(!$appointment->datetime)
            return $this->db->response->create(RESPONSE_FAIL, 'Date and time are required');

        //2. Inserts and returns the id
        return $this->db->response->create(RESPONSE_OK, 'Appointment created',
            $this->db->insert(
                "INSERT INTO appointment (employeeId, clientId, petId, datetime) VALUES (:employeeId, :clientId, :petId, :datetime)",
                [
                    ':employeeId' => $appointment->employeeId,
                    ':clientId' => $appointment->clientId,
                    ':petId' => $appointment->petId,
                    ':datetime' => ($appointment->datetime)? $appointment->datetime: null
                ]
            )
        );
    }

    /**
     * Updates the user and returns the number of updated rows.
     * @param $appointment
     * @return object
     */
    public function update($appointment) {

        $query = 'UPDATE appointment SET ';
        $params = [':id' => $appointment->id];

        //Required fields
        if (isset($appointment->employeeId)) {
            if(!$appointment->employeeId)
                return $this->db->response->create(RESPONSE_FAIL, 'Employee is required');

            $query.= 'employeeId=:employeeId,';
            $params[':employeeId'] = $appointment->employeeId;
        }
        if (isset($appointment->petId)) {
            if(!$appointment->petId)
                return $this->db->response->create(RESPONSE_FAIL, 'Pet is required');

            $query.= 'petId=:petId,';
            $params[':petId'] = $appointment->petId;
        }
        if (isset($appointment->datetime)) {
            if(!$appointment->datetime)
                return $this->db->response->create(RESPONSE_FAIL, 'Date and time are required');

            $query.= 'datetime=:datetime,';
            $params[':datetime'] = $appointment->datetime;
        }
        // Optional fields
        if (isset($appointment->pictureBefore) && $appointment->pictureBefore) {
            $query.= 'pictureBefore=:pictureBefore,';
            $params[':pictureBefore'] = $appointment->pictureBefore;
        }
        if (isset($appointment->pictureAfter) && $appointment->pictureAfter) {
            $query.= 'pictureAfter=:pictureAfter,';
            $params[':pictureAfter'] = $appointment->pictureAfter;
        }

        $query = rtrim($query, ',');
        $query.= ' WHERE id=:id';

        $res = $this->db->update($query, $params);

        return $this->db->response->create(RESPONSE_OK, 'Changes saved', $res);
    }
}