<?php

namespace City;

use Exception;
use City\Controllers\ApiController;

class CityRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'api':
                new ApiController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}