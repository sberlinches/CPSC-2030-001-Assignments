<?php

namespace City\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use City\Models\City;

class ApiController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'get_all_by_state':
                $this->getAllAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function getAllAction() {

        // Check if the URL params are present
        $stateId = $this->url->parameter;
        if (!$stateId)
            throw new Exception('Parameter missing');

        $cityModel = new City(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        echo json_encode($cityModel->getAllByStateId($stateId));
    }
}
