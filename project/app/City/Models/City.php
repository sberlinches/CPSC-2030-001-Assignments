<?php

namespace City\Models;

Use Classes\Database;

class City {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all cities by state.
     * @param $stateId
     * @return mixed
     */
    public function getAllByStateId($stateId) {

        return $this->db->getResults(
            "SELECT * FROM city WHERE stateId = :stateId",
            [':stateId' => $stateId]
        );
    }
}