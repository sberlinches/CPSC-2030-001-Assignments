<?php

namespace UserRoles\Models;

Use Classes\Database;

class UserRole {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all user roles.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT * FROM userRole"
        );
    }
}