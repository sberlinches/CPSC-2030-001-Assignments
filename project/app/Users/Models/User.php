<?php

namespace Users\Models;

Use Classes\Database;

class User {

    private $db;

    /**
     * User constructor.
     * @param Database $db
     */
    public function __construct(Database $db) {
        $this->db = $db;
    }

    /**
     * Gets and returns all users.
     * @return array
     */
    public function getAll() {

        return $this->db->getResults(
            "SELECT u.*, ur.name AS roleName
            FROM user u
            INNER JOIN userRole ur on u.roleId = ur.id"
        );
    }

    /**
     * Gets and returns an user by id.
     * @param $id
     * @return mixed
     */
    public function getById($id) {

        $user = $this->db->getResult(
            "SELECT u.*, ur.name AS roleName, c.name AS countryName, s.name AS stateName, ci.name AS cityName
            FROM user u
            INNER JOIN userRole ur on u.roleId = ur.id
            LEFT JOIN country c on u.countryId = c.id
            LEFT JOIN state s on u.stateId = s.id
            LEFT JOIN city ci on u.cityId = ci.id
            WHERE u.id = :id",
            [':id' => $id]
        );

        return $user;
    }

    /**
     * Gets and returns an user by email.
     * @param $email
     * @return mixed
     */
    public function getByEmail($email) {

        return $this->db->getResult(
            "SELECT * FROM user WHERE email = :email",
            [':email' => $email]
        );
    }

    /**
     * Gets and returns an user by role
     * @param $roleId
     * @param null $limit
     * @return mixed
     */
    public function getByRole($roleId, $limit = null) {

        $query = "SELECT * FROM user WHERE roleId = :roleId";

        if ($limit)
            $query .= " LIMIT " . $limit;

        return $this->db->getResults($query, [':roleId' => $roleId]);
    }

    /**
     * Inserts a and returns the id if the user was inserted.
     * @param $user
     * @return mixed
     */
    public function create($user) {

        //1. Checks the required fields
        if(!$user->firstName)
            return $this->db->response->create(RESPONSE_FAIL, 'First name is required');
        if(!$user->email)
            return $this->db->response->create(RESPONSE_FAIL, 'Email is required');
        if(!$user->password)
            return $this->db->response->create(RESPONSE_FAIL, 'Password is required');

        //2. Checks if the user already exists
        $res = $this->db->getResult(
            "SELECT * FROM user WHERE email = :email",
            [':email' => $user->email]
        );

        if($res)
            return $this->db->response->create(RESPONSE_FAIL, 'The Email already exists');

        //3. Inserts and returns the id
        return $this->db->response->create(RESPONSE_OK, 'User created',
            $this->db->insert(
                "INSERT INTO user (roleId, email, password, firstName, lastName) VALUES (:roleId, :email, :password, :firstName, :lastName)",
                [
                    ':roleId' => $user->roleId,
                    ':email' => strtolower($user->email),
                    ':password' => password_hash($user->password, PASSWORD_BCRYPT),
                    ':firstName' => $user->firstName,
                    ':lastName' => $user->lastName,
                ]
            )
        );
    }

    /**
     * Updates the user and returns the number of updated rows.
     * @param $user
     * @return object
     */
    public function update($user) {

        $query = 'UPDATE user SET ';
        $params = [':id' => $user->id];

        //1. Check fields and creates creates the query
        //Required fields
        if (isset($user->roleId)) {
            if(!$user->roleId)
                return $this->db->response->create(RESPONSE_FAIL, 'Role is required');

            $query.= 'roleId=:roleId,';
            $params[':roleId'] = $user->roleId;
        }
        if (isset($user->email)) { //TODO: Check if email already exists
            if(!$user->email)
                return $this->db->response->create(RESPONSE_FAIL, 'Email is required');

            $query.= 'email=:email,';
            $params[':email'] = strtolower($user->email);
        }
        if (isset($user->password)) {
            if(!$user->password)
                return $this->db->response->create(RESPONSE_FAIL, 'Password is required');

            $query.= 'password=:password,';
            $params[':password'] = password_hash($user->password, PASSWORD_BCRYPT);
        }
        if (isset($user->firstName)) {
            if(!$user->firstName)
                return $this->db->response->create(RESPONSE_FAIL, 'First name is required');

            $query.= 'firstName=:firstName,';
            $params[':firstName'] = $user->firstName;
        }
        // Optional fields
        if (isset($user->picture) && $user->picture) {
            $query.= 'picture=:picture,';
            $params[':picture'] = $user->picture;
        }
        if (isset($user->lastName) && $user->lastName) {
            $query.= 'lastName=:lastName,';
            $params[':lastName'] = $user->lastName;
        }
        if (isset($user->birthAt) && $user->birthAt) {
            $query.= 'birthAt=:birthAt,';
            $params[':birthAt'] = $user->birthAt;
        }
        if (isset($user->countryId)) {
            $query.= 'countryId=:countryId,';
            $params[':countryId'] = ($user->countryId)? $user->countryId: NULL;
        }
        if (isset($user->stateId)) {
            $query.= 'stateId=:stateId,';
            $params[':stateId'] = ($user->stateId)? $user->stateId: NULL;
        }
        if (isset($user->cityId)) {
            $query.= 'cityId=:cityId,';
            $params[':cityId'] = ($user->cityId)? $user->cityId: NULL;
        }
        if (isset($user->address) && $user->address) {
            $query.= 'address=:address,';
            $params[':address'] = $user->address;
        }

        $query = rtrim($query, ',');
        $query.= ' WHERE id=:id';

        $res = $this->db->update($query, $params);

        return $this->db->response->create(RESPONSE_OK, 'Changes saved', $res);
    }

    /**
     * Deletes an user from the DB and returns whether the entry was deleted or
     * not.
     * @param $id
     * @return object
     */
    public function delete($id) {

        $res = $this->db->delete(
            "DELETE FROM user WHERE id = :id",
            [':id' => $id]
        );

        if(!$res)
            return $this->db->response->create(RESPONSE_FAIL, 'The user could not be deleted', $res);

        return $this->db->response->create(RESPONSE_OK, 'User deleted', $res);
    }
}