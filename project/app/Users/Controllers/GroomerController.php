<?php

namespace Users\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Users\Models\User;

class GroomerController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Initializes DB, models and variables
        $userModel = new User(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        // Renders the view
        $this->render(
            $this->config->modules['users']['groomers']['list']->view, [
            'uid' =>  $this->config->modules['users']['groomers']['list']->uid,
            'title' => 'Our groomers',
            'employees' => $userModel->getByRole(EMPLOYEE)
        ]);
    }
}