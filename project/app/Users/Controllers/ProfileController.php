<?php

namespace Users\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Users\Models\User;
use UserRoles\Models\UserRole;

class ProfileController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            case 'new':
                $this->newAction();
                break;
            case 'show':
                $this->showAction();
                break;
            case 'edit':
                $this->editAction();
                break;
            case 'delete':
                $this->deleteAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Initializes DB, models and variables
        $userModel = new User(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        // Renders the view
        $this->render(
            $this->config->modules['users']['profile']['list']->view, [
            'uid' =>  $this->config->modules['users']['profile']['list']->uid,
            'title' => 'Users',
            'users' => $userModel->getAll()
        ]);
    }

    private function newAction() {

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $userModel = new User($db);
        $userRoleModel = new UserRole($db);

        // Stores in DB, login, and redirects
        if($_POST) {

            $user = (object)$_POST;

            //4.2. Checks if the form is valid and updates
            $response = $userModel->create($user);

            //3.1 If the creation succeed redirects
            if ($response->code === RESPONSE_OK)
                $this->redirect($this->config->modules['users']['profile']['list']->url);
        }

        // Renders the view
        $this->render(
            $this->config->modules['users']['profile']['new']->view, [
            'uid' => $this->config->modules['users']['profile']['new']->uid,
            'title' => 'New user',
            'user' => (isset($user))? $user: null,
            'userRoles' => $userRoleModel->getAll(),
            'response' => (isset($response))? $response: null
        ]);
    }

    private function showAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $userModel = new User(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));
        $user = $userModel->getById($userId);

        // Check if the user exists
        if(!$user)
            $this->render('error.html.twig', [
                'uid' => 'error',
                'message' => '404: Not found'
            ]);

        // Renders the view
        $this->render(
            $this->config->modules['users']['profile']['show']->view, [
                'uid' =>  $this->config->modules['users']['profile']['show']->uid,
                'title' => $user->firstName . '\'s profile',
                'userId' => $userId,
                'user' => $user
            ]);
    }

    private function editAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        //3. Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $userModel = new User($db);
        $userRoleModel = new UserRole($db);

        // Stores in DB and redirects
        if ($_POST || $_FILES) {

            $_POST['id'] = $userId;
            $user = (object) $_POST;
            $update = true;

            // 4.1 Uploads the picture (If there's any)
            if(!$_FILES["picture"]['error']) {
                $response = $this->uploadFile($_FILES["picture"], ['image/jpeg'], UPLOADS_PATH);
                $user->picture = $response->data;
                $update = ($response->code === RESPONSE_OK);
            }

            if($update) {
                //4.2. Checks if the form is valid and updates
                $response = $userModel->update($user);

                //4.3 If the update succeed redirects, otherwise it will show errors
                if ($response->code === RESPONSE_OK)
                    $this->redirect($this->config->modules['users']['profile']['show']->url . $user->id);
            }

        } else
            $user = $userModel->getById($userId);

        // Check if the user exists
        if(!$user)
            $this->render('error.html.twig', [
                'uid' => 'error',
                'message' => '404: Not found'
            ]);

        // Renders the view
        $this->render(
            $this->config->modules['users']['profile']['edit']->view, [
            'uid' =>  $this->config->modules['users']['profile']['edit']->uid,
            'title' => 'Edit ' . $user->firstName . '\'s profile',
            'userId' => $userId,
            'user' => $user,
            'userRoles' => $userRoleModel->getAll(),
            'response' => (isset($response))? $response: null
        ]);
    }

    private function deleteAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $userModel = new User(new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        ));

        $response = $userModel->delete($userId);

        if($response->code === RESPONSE_OK) {
            if ($this->userSession->roleId === ADMIN)
                $this->redirect($this->config->modules['users']['profile']['list']->url);
            else
                $this->redirect($this->config->modules['authentication']['logout']['index']->url);
        }
    }
}
