<?php

namespace Users\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Appointments\Models\Appointment;
use Users\Models\User;

class AppointmentController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $appointmentsModel = new Appointment($db);
        $userModel = new User($db);
        $user = $userModel->getById($userId);

        //4. Renders the view
        $this->render(
            $this->config->modules['users']['appointments']['list']->view, [
            'uid' =>  $this->config->modules['users']['appointments']['list']->uid,
            'title' => $user->firstName . '\'s appointments',
            'userId' => $userId,
            'appointments' => $appointmentsModel->getByUserId($userId)
        ]);
    }
}