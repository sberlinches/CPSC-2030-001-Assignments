<?php

namespace Users\Controllers;

use Exception;
use Classes\Controller;
use Classes\Database;
use Pets\Models\Pet;
use Users\Models\User;

class PetController extends Controller {

    public function __construct($url) {

        parent::__construct($url);

        switch ($this->url->action) {
            case 'list':
                $this->listAction();
                break;
            default:
                throw new Exception( $this->url->action . 'Action not registered');
        }
    }

    private function listAction() {

        // Check if the URL params are present
        $userId = $this->url->parameter;
        if (!$userId)
            throw new Exception('Parameter missing');

        // Initializes DB, models and variables
        $db = new Database(
            $this->config->db['mysql']->host,
            $this->config->db['mysql']->dbName,
            $this->config->db['mysql']->username,
            $this->config->db['mysql']->password
        );
        $petModel = new Pet($db);
        $userModel = new User($db);
        $user = $userModel->getById($userId);

        // Renders the view
        $this->render(
            $this->config->modules['users']['pets']['list']->view, [
            'uid' =>  $this->config->modules['users']['pets']['list']->uid,
            'title' => $user->firstName . '\'s pets',
            'userId' => $userId,
            'pets' => $petModel->getByOwnerId($userId)
        ]);
    }
}