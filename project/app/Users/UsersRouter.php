<?php

namespace Users;

use Exception;
use Users\Controllers\ProfileController;
use Users\Controllers\AppointmentController;
use Users\Controllers\PetController;
use Users\Controllers\GroomerController;

class UsersRouter {

    function __construct($url) {

        switch ($url->controller) {
            case 'profile':
                new ProfileController($url);
                break;
            case 'appointments':
                new AppointmentController($url);
                break;
            case 'pets':
                new PetController($url);
                break;
            case 'groomers':
                new GroomerController($url);
                break;
            default:
                throw new Exception( $url->controller . 'Controller not registered');
        }
    }
}