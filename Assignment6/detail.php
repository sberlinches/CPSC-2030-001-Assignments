<?php

require 'loader.php';

if (!isset($_GET['id'])) {
    echo 'Parameters missing. Check the URL';
    exit;
}

$id = (int) $_GET['id'];

if (!$id ) {
    echo 'Parameters missing. Check the URL';
    exit;
}

$pokemon = $db->getResult($config->procedures->getById, [':id' => $id]);
$quirks = $db->getResults($config->procedures->getQuirksById, [':id' => $id]);
$bodyTitle = 'Pokédex';
$headTitle = $bodyTitle . ' | ' . $pokemon->name;

echo $twig->render('detail.html.twig', [
    'headTitle' => $headTitle,
    'bodyTitle' => $bodyTitle,
    'pokemon' => $pokemon,
    'quirks' => $quirks
]);
