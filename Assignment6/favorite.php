<?php

require 'loader.php';

if (!isset($_GET['action']) || !isset($_GET['id'])) {
    echo 'Parameters missing. Check the URL';
    exit;
}

$action = $_GET['action'];
$id     = (int) $_GET['id'];

if (!$action || !$id ) {
    echo 'Parameters missing. Check the URL';
    exit;
}

if ($action === 'add')
    array_push($_SESSION['favorites'], $id);
elseif ($action === 'remove')
    unset($_SESSION['favorites'][array_search($id, $_SESSION['favorites'])]);

header('Location:'.$_SERVER['HTTP_REFERER'], true, 301);