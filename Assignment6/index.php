<?php

require 'loader.php';

$favoriteIDs = implode(',', $_SESSION['favorites']);
$type = (isset($_GET['type']))? $_GET['type']: null;
$pokemons = ($type)?
    $db->getResults($config->procedures->getAllByType, ['type' => $type]):
    $db->getResults($config->procedures->getAll);
$favorites = ($favoriteIDs)? $db->getResults($config->procedures->getAllByIds, ['ids' => $favoriteIDs]): null;
$popularGen4 = $db->getResults($config->procedures->getAllPopularFromGeneration, ['gen' => 4]);
$bodyTitle = 'Pokédex';
$headTitle = $bodyTitle . ' | ';
$headTitle.= ($type)? $type: 'All';

echo $twig->render('main.html.twig', [
    'headTitle' => $headTitle,
    'bodyTitle' => $bodyTitle,
    'type' => $type,
    'pokemons' => $pokemons,
    'favorites' => $favorites,
    'popularGen4' => $popularGen4
]);
