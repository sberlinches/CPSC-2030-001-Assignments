<?php

require 'classes/Database.php';
require 'vendor/autoload.php';

$config = include('config.php');
$db     = new Database($config->db);
$loader = new Twig_Loader_Filesystem(TEMPLATES_ROOT);
$twig   = new Twig_Environment($loader);

$twig->addFilter(new Twig_Filter('json_decode', function ($string) {
    return json_decode($string);
}));

$twig->addFilter(new Twig_Filter('percentage', function ($value, $max) {
    return ($value/$max)*100;
}));

$twig->addFilter(new Twig_Filter('in_favorites', function ($value) {
    return in_array($value,  $_SESSION['favorites']);
}));

session_start();

if (!isset($_SESSION['favorites']))
    $_SESSION['favorites'] = [];