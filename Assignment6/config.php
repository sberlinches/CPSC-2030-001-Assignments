<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define("TEMPLATES_ROOT", 'templates' . DIRECTORY_SEPARATOR);

return (object) [
    'db' => (object) [
        'host' => 'localhost',
        'dbName' => 'pokedex',
        'username' => "CPSC2030",
        'password' => "CPSC2030",
    ],
    'procedures' => (object) [
        'getAll' => 'CALL pokedex.getAll()',
        'getAllByIds' => 'CALL pokedex.getAllByIds(:ids)',
        'getAllByType' => 'CALL pokedex.getAllByType(:type)',
        'getById' => 'CALL pokedex.getById(:id)',
        'getQuirksById' => 'CALL pokedex.getQuirksById(:id)',
        'getAllPopularFromGeneration' => 'CALL pokedex.getAllPopularFromGeneration(:gen)',
    ]
];
